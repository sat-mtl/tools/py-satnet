# Contributing


## IDE

This project is configured and developed with PyCharm 2017. While it is not necessary to use PyCharm, a lot of useful
stuff (run/debug configurations, scopes, etc.) is already configured and ready to use.

## Code Style & Conventions

This project uses Python 3.5 and follows the [PEP8 style guide](https://www.python.org/dev/peps/pep-0008/).

It is mandatory that reusable code uses [type hints](https://www.python.org/dev/peps/pep-0008/). Since Python 3.5 does
not yet supports [PEP 526](https://www.python.org/dev/peps/pep-0526) (variable annotation), use the `# type: Something`
comment annotations on variables. Example:

    property = None  # type: str
    
In case static type checking reports false negatives, type checking can be ignored with `# type: ignore`.

    some.thing(complains="Without reason")  # type: ignore


## Linting

Since we use PyCharm, no linting was setup for this project. 

If you add/use linting outside of PyCharm, please document your method here.


## Static Analysis

[Mypy](http://mypy-lang.org/) is used for static type checking. 

Install mypy using the latest github version:

    python3 -m pip install -U git+git://github.com/python/mypy.git

Use the following to run mypy:

    MYPYPATH=lib/satlib:lib/satnet:lib/eis mypy --strict client|server
    
Use either `client` or `server` to check one or the other. If you want to have both checked at the same time, you can
run `./tools/type_check.sh` from the project root directory. It will run both client and server and remove duplicate
entries from shared code, but it will be slower since it has to run both and a lot of code is shared.


## Unit Tests

Tests are written using Python's `unittest` and are run from inside PyCharm with the **All Tests** run/debug 
configuration. If you are not using PyCharm and use the unit tests please update this document with instructions on how 
to use them.


## UML

You can find various UML diagrams in `doc/uml`. They are made using PyCharm's [PlantUML](http://plantuml.com/) plugin.
If you are not using PyCharm and use the UML diagram, please update this document with instructions on how to use it.
