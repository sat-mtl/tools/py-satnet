from satnet.request import Request, Response, request, response, ClientRequest, ClientResponse, ServerRequest, ServerResponse, ResponseCallback
from .base import MockSession


@request(id=0xF0)
class MockRequest(Request[MockSession]):
    def handle(self, session: MockSession, respond: ResponseCallback):
        respond(MockResponse())


@request(id=0xF1)
class MockClientRequest(ClientRequest[MockSession]):
    def handle(self, session: MockSession, respond: ResponseCallback):
        respond(MockServerResponse())


@request(id=0xF2)
class MockServerRequest(ServerRequest[MockSession]):
    def handle(self, session: MockSession, respond: ResponseCallback):
        respond(MockClientResponse())


@response(id=0xF0)
class MockResponse(Response[MockSession, MockRequest]):
    def handle(self, request: MockRequest, session: MockSession):
        pass


@response(id=0xF1)
class MockClientResponse(ClientResponse[MockSession, MockServerRequest]):
    def handle(self, request: MockServerRequest, session: MockSession):
        pass


@response(id=0xF2)
class MockServerResponse(ServerResponse[MockSession, MockClientRequest]):
    def handle(self, request: MockClientRequest, session: MockSession):
        pass
