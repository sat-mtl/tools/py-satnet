from typing import Any

from satnet.client import Client, ClientAdapter, LocalSession
from satnet.message import InternalMessageId


class MockLocalSession(LocalSession):
    pass


class MockClient(Client):
    def __init__(self):
        super().__init__(
            session=MockLocalSession(client=self),
            adapter=MockClientAdapter()
        )


class MockClientAdapter(ClientAdapter):
    def stop(self):
        pass

    def start(self):
        pass

    def run(self, now: float, dt: float):
        pass

    def _on_internal_message(self, message_id: InternalMessageId, payload: Any):
        pass
