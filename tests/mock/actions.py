from satnet.action import Action, action


class DummySession:
    pass


@action(id=0xF0)
class MockAction(Action[DummySession]):

    def apply(self, session: DummySession) -> None:
        pass

    def revert(self, session: DummySession) -> None:
        pass
