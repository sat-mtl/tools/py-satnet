from unittest import TestCase, mock

from satnet.message import Message
from .mock.actions import MockAction
from .mock.base import MockSession, MockBase, MockBaseAdapter
from .mock.client import MockClient
from .mock.request import MockRequest
from .mock.session import MockSession


class TestSession(TestCase):
    def setUp(self):
        self.net = MockBase(adapter=MockBaseAdapter())

    def create_session(self):
        return MockSession(net=self.net)

    def test_request(self):
        session = self.create_session()
        with mock.patch.object(session, 'send') as send:
            request = MockRequest()
            session.request(request)
            self.assertEqual(send.call_count, 1)
            self.assertIsInstance(send.call_args[0][0], Message)
            msg = send.call_args[0][0]
            self.assertEqual(msg.data, request)

    def test_request_not_request(self):
        session = self.create_session()
        with self.assertRaises(AssertionError), mock.patch.object(session, 'send') as send:
            session.request("something else")
            send.assert_not_called()

    def test_server_request_not_client_request(self):
        session = self.create_session()
        with self.assertRaises(AssertionError), mock.patch.object(session, 'send') as send:
            session.request(MockRequest())
            send.assert_not_called()

            # TODO: Command
            # TODO: Notify


class TestLocalSession(TestCase):
    def setUp(self):
        self.client = MockClient()

    def test_action(self):
        with mock.patch.object(self.client.session, 'send') as send:
            action = MockAction()
            self.client.session.action(action)
            self.assertEqual(send.call_count, 1)
            self.assertIsInstance(send.call_args[0][0], Message)
            msg = send.call_args[0][0]
            self.assertEqual(msg.data, action)

    def test_action_not_action(self):
        with self.assertRaises(AssertionError), mock.patch.object(self.client.session, 'send') as send:
            self.client.session.action("something else")
            send.assert_not_called()
