import uuid
from unittest import TestCase, mock

from satnet import SerializablePrefix
from satnet.action import ActionHistory
from satnet.message import Message, InternalMessageId
from satnet.request import Response
from satnet.serialization import Serializable
from satnet.server import Server
from .mock.actions import MockAction
from .mock.base import MockSession
from .mock.commands import MockCommand, MockClientCommand, MockServerCommand
from .mock.notification import MockNotification
from .mock.request import MockRequest, MockResponse, MockClientRequest, MockClientResponse, MockServerRequest, MockServerResponse
from .mock.server import MockServer, MockServerAdapter, MockRemoteClientAdapter, MockRemoteSession


class TestRemoteSession(TestCase):
    def setUp(self):
        self.adapter = MockServerAdapter()
        self.server = MockServer(self.adapter)

    def create_rc(self):
        return MockRemoteSession(id=0x00, server=self.server)

    def test_initialization(self):
        rc = self.create_rc()
        self.assertEqual(rc._id, 0x00)
        self.assertEqual(rc._net, self.server)

    def test_str(self):
        rc = self.create_rc()
        self.assertEqual(str(rc), str(0x00))

    def test_id_property(self):
        rc = self.create_rc()
        self.assertEqual(rc.id, 0x00)

    def test_server_property(self):
        rc = self.create_rc()
        self.assertEqual(rc.server, self.server)

    def test_disconnect(self):
        with mock.patch.object(self.server, 'disconnect') as srv_dis:
            rc = self.create_rc()
            rc.disconnect()
            srv_dis.assert_called_once_with(rc)

    def test_send(self):
        with mock.patch.object(self.server, 'send') as srv_snd:
            rc = self.create_rc()
            msg = Message()
            rc.send(msg)
            self.assertEqual(msg.session, rc)
            srv_snd.assert_called_once_with(msg)

    def test_send_not_message(self):
        rc = self.create_rc()
        with self.assertRaises(AssertionError), mock.patch.object(self.server, 'send') as srv_snd:
            rc.send("something else")
            srv_snd.assert_not_called()

    def test_command(self):
        rc = self.create_rc()
        with mock.patch.object(rc, 'send') as rc_snd:
            cmd = MockCommand()
            rc.command(cmd)
            self.assertEqual(rc_snd.call_count, 1)
            self.assertIsInstance(rc_snd.call_args[0][0], Message)
            msg = rc_snd.call_args[0][0]
            self.assertEqual(msg.data, cmd)

    def test_command_not_command(self):
        rc = self.create_rc()
        with self.assertRaises(AssertionError), mock.patch.object(rc, 'send') as rc_snd:
            rc.command("something else")
            rc_snd.assert_not_called()

    def test_request(self):
        rc = self.create_rc()
        with mock.patch.object(rc, 'send') as rc_snd, \
                mock.patch.object(self.server, 'get_request', wraps=self.server.get_request) as srv_req:
            req = MockRequest()
            rc.request(req)

            # Server get_request
            srv_req.assert_called_once_with(req)

            # RC send
            self.assertEqual(rc_snd.call_count, 1)
            self.assertIsInstance(rc_snd.call_args[0][0], Message)
            msg = rc_snd.call_args[0][0]
            self.assertEqual(msg.data, req)

    def test_request_not_request(self):
        rc = self.create_rc()
        with self.assertRaises(AssertionError), \
             mock.patch.object(rc, 'send') as rc_snd, \
                mock.patch.object(self.server, 'get_request', wraps=self.server.get_request) as srv_req:
            rc.request("something else")
            rc_snd.assert_not_called()
            srv_req.assert_not_called()

    def test_client_request_not_server_request(self):
        rc = self.create_rc()
        with self.assertRaises(AssertionError), \
             mock.patch.object(rc, 'send') as rc_snd, \
                mock.patch.object(self.server, 'get_request', wraps=self.server.get_request) as srv_req:
            rc.request(MockRequest)
            rc_snd.assert_not_called()
            srv_req.assert_not_called()

    def test_notify_not_notification(self):
        rc = self.create_rc()
        with self.assertRaises(AssertionError), mock.patch.object(rc, 'send') as send:
            rc.notify("something else")
            send.assert_not_called()

    def test_notification_no_context_both(self):
        rc = self.create_rc()
        rc.subscribe(topic=0xF0, context=None)
        notification = MockNotification()
        with mock.patch.object(rc, 'send') as send:
            rc.notify(notification)
            self.assertEqual(send.call_count, 1)
            self.assertIsInstance(send.call_args[0][0], Message)
            msg = send.call_args[0][0]
            self.assertEqual(msg.data, notification)

    def test_notification_no_subscription_context_and_notification_context(self):
        rc = self.create_rc()
        rc.subscribe(topic=0xF0, context=None)
        context = uuid.uuid4()
        notification = MockNotification(context=context)
        with mock.patch.object(rc, 'send') as send:
            rc.notify(notification)
            self.assertEqual(send.call_count, 1)
            self.assertIsInstance(send.call_args[0][0], Message)
            msg = send.call_args[0][0]
            self.assertEqual(msg.data, notification)

    def test_notification_subscription_context_and_notification_context(self):
        rc = self.create_rc()
        context = uuid.uuid4()
        rc.subscribe(topic=0xF0, context=context)
        notification = MockNotification(context=context)
        with mock.patch.object(rc, 'send') as send:
            rc.notify(notification)
            self.assertEqual(send.call_count, 1)
            self.assertIsInstance(send.call_args[0][0], Message)
            msg = send.call_args[0][0]
            self.assertEqual(msg.data, notification)

    def test_notification_subscription_context_and_no_notification_context(self):
        rc = self.create_rc()
        context = uuid.uuid4()
        rc.subscribe(topic=0xF0, context=context)
        notification = MockNotification()
        with mock.patch.object(rc, 'send') as send:
            rc.notify(notification)
            send.assert_not_called()

    def test_unsubscription1(self):
        rc = self.create_rc()
        rc.subscribe(topic=0xF0, context=None)
        rc.unsubscribe(topic=0xF0, context=None)
        with mock.patch.object(rc, 'send') as send:
            rc.notify(MockNotification())
            send.assert_not_called()

    def test_unsubscription2(self):
        rc = self.create_rc()
        context = uuid.uuid4()
        rc.subscribe(topic=0xF0, context=context)
        rc.unsubscribe(topic=0xF0, context=None)
        with mock.patch.object(rc, 'send') as send:
            rc.notify(MockNotification())
            send.assert_not_called()

    def test_unsubscription3(self):
        rc = self.create_rc()
        context = uuid.uuid4()
        rc.subscribe(topic=0xF0, context=context)
        rc.unsubscribe(topic=0xF0, context=context)
        with mock.patch.object(rc, 'send') as send:
            rc.notify(MockNotification())
            send.assert_not_called()

    def test_unsubscription4(self):
        rc = self.create_rc()
        context1 = uuid.uuid4()
        context2 = uuid.uuid4()
        rc.subscribe(topic=0xF0, context=context1)
        rc.subscribe(topic=0xF0, context=context2)
        rc.unsubscribe(topic=0xF0, context=context2)
        notification1 = MockNotification()
        notification2 = MockNotification(context=context1)
        notification3 = MockNotification(context=context2)
        with mock.patch.object(rc, 'send') as send:
            rc.notify(notification1)
            rc.notify(notification2)
            rc.notify(notification3)
            self.assertEqual(send.call_count, 1)
            self.assertIsInstance(send.call_args_list[0][0][0], Message)
            msg = send.call_args_list[0][0][0]
            self.assertEqual(msg.data, notification2)

    def test_unsubscription5(self):
        rc = self.create_rc()
        context1 = uuid.uuid4()
        context2 = uuid.uuid4()
        rc.subscribe(topic=0xF0)
        rc.subscribe(topic=0xF0, context=context2)
        rc.unsubscribe(topic=0xF0, context=context2)
        notification1 = MockNotification()
        notification2 = MockNotification(context=context1)
        notification3 = MockNotification(context=context2)
        with mock.patch.object(rc, 'send') as send:
            rc.notify(notification1)
            rc.notify(notification2)
            rc.notify(notification3)
            self.assertEqual(send.call_count, 3)
            self.assertIsInstance(send.call_args_list[0][0][0], Message)
            msg = send.call_args_list[0][0][0]
            self.assertEqual(msg.data, notification1)
            self.assertIsInstance(send.call_args_list[1][0][0], Message)
            msg = send.call_args_list[1][0][0]
            self.assertEqual(msg.data, notification2)
            self.assertIsInstance(send.call_args_list[2][0][0], Message)
            msg = send.call_args_list[2][0][0]
            self.assertEqual(msg.data, notification3)


class TestRemoteClientAdapter(TestCase):
    def setUp(self):
        self.session = MockSession()
        self.adapter = MockServerAdapter()
        self.server = MockServer(self.adapter)
        MockRemoteClientAdapter._id_counter = 0

    def test_id_generation(self):
        self.assertEqual(MockRemoteClientAdapter._id_counter, 0)
        id = MockRemoteClientAdapter.get_id()
        self.assertEqual(MockRemoteClientAdapter._id_counter, 1)
        self.assertEqual(id, 1)

    def test_id_assignation(self):
        rc_adapter = MockRemoteClientAdapter(self.adapter)
        self.assertEqual(rc_adapter.id, 1)


class TestServer(TestCase):
    def setUp(self):
        self.session = MockSession()
        self.adapter = MockServerAdapter()

    def create_server(self):
        return Server(self.adapter, session_class=MockRemoteSession)

    def test_default_client_factory(self):
        server = self.create_server()
        rc_adapter = MockRemoteClientAdapter(self.adapter)
        rc = server.default_session_factory(rc_adapter)
        self.assertIsInstance(rc, MockRemoteSession)

    def test_default_client_factory_fail_without_client_class(self):
        server = Server(self.adapter)
        rc_adapter = MockRemoteClientAdapter(self.adapter)
        with self.assertRaisesRegex(Exception, "A session class is required"):
            rc = server.default_session_factory(rc_adapter)

    def test_start(self):
        server = self.create_server()
        with mock.patch.object(server, '_run') as srv_run:
            server.start(123)
            self.assertEqual(server.adapter.port, 123)
            srv_run.assert_called_once_with()

    def test_stop(self):
        server = self.create_server()
        with mock.patch.object(server, '_close') as srv_close:
            server.stop()
            srv_close.assert_called_once_with()

    def test_step(self):
        server = self.create_server()
        server._sessions = {0: mock.Mock(), 1: mock.Mock(), 2: mock.Mock()}
        now = 1.2343
        dt = 5.678

        with mock.patch.object(server._sessions[0], 'step') as s1, \
                mock.patch.object(server._sessions[1], 'step') as s2, \
                mock.patch.object(server._sessions[2], 'step') as s3:
            server.step(now, dt)
            s1.assert_called_once_with(now, dt)
            s2.assert_called_once_with(now, dt)
            s3.assert_called_once_with(now, dt)

    def test_disconnect(self):
        server = self.create_server()
        rc_adapter = MockRemoteClientAdapter(self.adapter)
        rc = server.default_session_factory(rc_adapter)
        with mock.patch.object(self.adapter, 'send_internal') as si:
            server.disconnect(rc)
            si.assert_called_once_with((InternalMessageId.SESSION_DISCONNECT, rc.id))

    def test_register_action_context(self):
        server = self.create_server()
        cid = uuid.uuid4()
        context = server.register_action_context(cid)
        self.assertIsNotNone(context)
        self.assertIsInstance(context, ActionHistory)

    def test_get_action_context(self):
        server = self.create_server()
        cid = uuid.uuid4()
        context1 = server.register_action_context(cid)
        context2 = server.get_action_context(cid)
        self.assertIsNotNone(context2)
        self.assertIsInstance(context2, ActionHistory)
        self.assertEqual(context1, context2)

    def test_register_action_context_fail_if_already_exists(self):
        server = self.create_server()
        cid = uuid.uuid4()
        server.register_action_context(cid)
        context1 = server.get_action_context(cid)
        with self.assertRaisesRegex(Exception, "Context already registered"):
            server.register_action_context(cid)
        # Default context should not have been removed
        context2 = server.get_action_context(cid)
        self.assertIsNotNone(context2)
        self.assertIsInstance(context2, ActionHistory)
        self.assertEqual(context1, context2)

    def test_unregister_action_context(self):
        server = self.create_server()
        cid = uuid.uuid4()
        server.register_action_context(cid)
        server.unregister_action_context(cid)
        context = server.get_action_context(cid)
        self.assertIsNone(context)

    def test_unregister_action_context_fail_if_dont_exists(self):
        server = self.create_server()
        cid = uuid.uuid4()
        with self.assertRaisesRegex(Exception, "Context not registered"):
            server.unregister_action_context(cid)

    def test_request(self):
        server = self.create_server()
        server._sessions = {
            0: MockRemoteSession(id=0x01, server=server),
            1: MockRemoteSession(id=0x02, server=server),
            2: MockRemoteSession(id=0x03, server=server)
        }
        requests = [MockRequest(), MockRequest(), MockRequest()]
        get_request = mock.Mock(side_effect=requests)
        with mock.patch.object(server._sessions[0], 'request') as r1, \
                mock.patch.object(server._sessions[1], 'request') as r2, \
                mock.patch.object(server._sessions[2], 'request') as r3:
            server.request_all(get_request)
            r1.assert_called_once_with(requests[0])
            r2.assert_called_once_with(requests[1])
            r3.assert_called_once_with(requests[2])

    def test_command(self):
        server = self.create_server()
        server._sessions = {
            0: MockRemoteSession(id=0x01, server=server),
            1: MockRemoteSession(id=0x02, server=server),
            2: MockRemoteSession(id=0x03, server=server)
        }
        command = MockCommand()
        with mock.patch.object(server._sessions[0], 'command') as c1, \
                mock.patch.object(server._sessions[1], 'command') as c2, \
                mock.patch.object(server._sessions[2], 'command') as c3:
            server.command_all(command)
            c1.assert_called_once_with(command)
            c2.assert_called_once_with(command)
            c3.assert_called_once_with(command)

    def test_notify(self):
        server = self.create_server()
        server._sessions = {
            0: MockRemoteSession(id=0x01, server=server),
            1: MockRemoteSession(id=0x02, server=server),
            2: MockRemoteSession(id=0x03, server=server)
        }
        notification = MockNotification()
        with mock.patch.object(server._sessions[0], 'notify') as n1, \
                mock.patch.object(server._sessions[1], 'notify') as n2, \
                mock.patch.object(server._sessions[2], 'notify') as n3:
            server.notify_all(notification)
            n1.assert_called_once_with(notification)
            n2.assert_called_once_with(notification)
            n3.assert_called_once_with(notification)

    # region on_message

    def test_on_message_not_message(self):
        server = self.create_server()
        with self.assertRaises(AssertionError):
            server._on_message("something else")

    def test_on_message_unhandled(self):
        server = self.create_server()
        session = MockRemoteSession(id=0x01, server=server)
        server._sessions = {0: session}
        data = Serializable()
        data.serialization_prefix = SerializablePrefix.CUSTOM
        message = Message(data=data)
        message.session_id = 0x00
        with mock.patch.object(session, 'on_message') as session_on_message:
            server._on_message(message)
            self.assertEqual(message.session, session)
            session_on_message.assert_called_once_with(message)

    def test_on_message_client_command(self):
        server = self.create_server()
        rc = MockRemoteSession(id=0x01, server=server)
        server._sessions = {0: rc}
        command = MockClientCommand()
        message = Message(command)
        message.session_id = 0x00
        with mock.patch.object(command, 'handle') as ch:
            server._on_message(message)
            ch.assert_called_once_with(session=rc)

    def test_on_message_server_command(self):
        server = self.create_server()
        rc = MockRemoteSession(id=0x01, server=server)
        server._sessions = {0: rc}
        command = MockServerCommand()
        message = Message(command)
        message.session_id = 0x00
        with mock.patch.object(command, 'handle') as ch:
            self.assertRaises(AssertionError, server._on_message, message)
            ch.assert_not_called()

    def test_on_message_client_request(self):
        server = self.create_server()
        session = MockRemoteSession(id=0x01, server=server)
        server._sessions = {0: session}
        request = MockClientRequest()
        message = Message(request)
        message.session_id = 0x00
        with mock.patch.object(request, 'handle', wraps=request.handle) as rh, \
                mock.patch.object(session, 'send') as session_send:
            server._on_message(message)
            rh.assert_called_once_with(session=session, respond=mock.ANY)
            self.assertEqual(session_send.call_count, 1)

            msg = session_send.call_args[0][0]
            self.assertIsNotNone(msg)
            self.assertIsInstance(msg, Message)
            response = msg.data
            self.assertIsNotNone(response)
            self.assertIsInstance(response, Response)
            self.assertEqual(response, response)
            self.assertEqual(response.request_id, request.id)

    def test_on_message_server_request(self):
        server = self.create_server()
        session = MockRemoteSession(id=0x01, server=server)
        server._sessions = {0: session}
        request = MockServerRequest()
        message = Message(request)
        message.session_id = 0x00
        with mock.patch.object(request, 'handle') as rh, \
                mock.patch.object(session, 'send') as session_send:
            self.assertRaises(AssertionError, server._on_message, message)
            rh.assert_not_called()
            session_send.assert_not_called()

    def test_on_message_client_response(self):
        server = self.create_server()
        rc = MockRemoteSession(id=0x01, server=server)
        server._sessions = {0: rc}

        # Fake that we have a pending request
        request = MockServerRequest()
        server._requests[0x01] = request

        response = MockClientResponse(request_id=0x01)
        message = Message(response)
        message.session_id = 0x00

        with mock.patch.object(response, 'handle') as rh:
            server._on_message(message)
            rh.assert_called_once_with(request=request, session=rc)

    def test_on_message_server_response(self):
        server = self.create_server()
        rc = MockRemoteSession(id=0x01, server=server)
        server._sessions = {0: rc}

        # Fake that we have a pending request
        request = MockServerRequest()
        server._requests[0x01] = request

        response = MockServerResponse(request_id=0x01)
        message = Message(response)
        message.session_id = 0x00

        with mock.patch.object(response, 'handle') as rh:
            self.assertRaises(AssertionError, server._on_message, message)
            rh.assert_not_called()

    def test_on_message_action(self):
        server = self.create_server()
        rc = MockRemoteSession(id=0x01, server=server)
        server._sessions = {0: rc}

        action = MockAction()

        message = Message(action)
        message.session_id = 0x00

        with mock.patch.object(server.get_action_context(), 'apply') as apply:
            server._on_message(message)
            apply.assert_called_once_with(action=action, session=rc)

    # endregion

    # region on_internal_message

    def test_on_internal_message_client_connected(self):
        server = self.create_server()
        rc_adapter = MockRemoteClientAdapter(self.adapter)
        client = MockRemoteSession(id=rc_adapter.id, server=server)
        with mock.patch.object(server, '_session_factory', side_effect=[client]) as sf:
            server._on_internal_message(InternalMessageId.SESSION_CONNECTED, rc_adapter)
            sf.assert_called_once_with(rc_adapter)
            self.assertEqual(server._sessions[rc_adapter.id], client)

    def test_on_internal_message_client_connected_not_adapter(self):
        server = self.create_server()
        with self.assertRaises(AssertionError), mock.patch.object(server, '_session_factory') as sf:
            server._on_internal_message(InternalMessageId.SESSION_CONNECTED, "something else")
            sf.assert_not_called()

    def test_on_internal_message_client_disconnected(self):
        server = self.create_server()
        rc_adapter = MockRemoteClientAdapter(self.adapter)
        rc = MockRemoteSession(id=rc_adapter.id, server=server)
        server._sessions[rc_adapter.id] = rc
        self.assertEqual(len(server.sessions), 1)
        server._on_internal_message(InternalMessageId.SESSION_DISCONNECTED, rc_adapter)
        self.assertNotIn(rc_adapter.id, server._sessions)
        self.assertIsNone(server._sessions.get(rc_adapter.id))
        self.assertEqual(len(server.sessions), 0)

    def test_on_internal_message_client_disconnected_not_adapter(self):
        server = self.create_server()
        with self.assertRaises(AssertionError), mock.patch.object(server, '_session_factory') as sf:
            server._on_internal_message(InternalMessageId.SESSION_DISCONNECTED, "something else")
            sf.assert_not_called()

    # endregion
