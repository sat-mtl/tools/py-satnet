from unittest import TestCase

from satnet.errors import ParsingError
from satnet.message import Message, MessageDataType, MessageParser
from .mock.serializable import DummySerializable


class TestMessage(TestCase):

    def test_message(self):
        s = DummySerializable()
        m = Message(data=s, raw=b'RAW', session=b'CLIENT')
        self.assertEqual(m.data, s)
        self.assertEqual(m.raw, b'RAW')
        self.assertEqual(m.session, b'CLIENT')
        
    def test_serialization(self):
        m = Message(raw=b'RAW', session=b'CLIENT')
        self.assertEqual(MessageParser.to_bytes(m), bytes([MessageDataType.RAW.value]) + b'RAW')

    def test_deserialization(self):
        m = MessageParser.from_bytes(bytes([MessageDataType.RAW.value]) + b'RAW')
        self.assertEqual(m.raw, b'RAW')
        self.assertIsNone(m.session)

    def test_empty_serialization(self):
        with self.assertRaisesRegex(ParsingError, 'Nothing to encode'):
            MessageParser.to_bytes(Message())

    def test_raw_serialization(self):
        m = Message(raw=b'RAW')
        self.assertEqual(MessageParser.to_bytes(m), bytes([MessageDataType.RAW.value]) + b'RAW')

    def test_data_serialization(self):
        s = DummySerializable()
        m = Message(data=s)
        self.assertEqual(MessageParser.to_bytes(m), bytes([MessageDataType.SERIALIZED.value]) + s.serialize())

    def test_empty_deserialization(self):
        with self.assertRaisesRegex(ParsingError, 'Message too short'):
            MessageParser.from_bytes(bytes([]))

    def test_raw_deserialization(self):
        m = MessageParser.from_bytes(bytes([MessageDataType.RAW.value]) + b'RAW')
        self.assertIsNotNone(m)
        self.assertEqual(m.raw, b'RAW')
        self.assertIsNone(m.data)
        self.assertIsNone(m.session)

    def test_data_deserialization(self):
        s = DummySerializable()
        s.name = "Pouet"
        m = MessageParser.from_bytes(bytes([MessageDataType.SERIALIZED.value]) + s.serialize())
        self.assertIsNotNone(m)
        self.assertEqual(m.raw, s.serialize())
        self.assertIsInstance(m.data, DummySerializable)
        self.assertEqual(m.data.name, s.name)
        self.assertIsNone(m.session)

    def test_too_short(self):
        with self.assertRaisesRegex(ParsingError, 'Message too short'):
            MessageParser.from_bytes(b'')

    def test_invalid_data_type(self):
        # If 0xFF ever become valid we'll need to change this test
        with self.assertRaisesRegex(ParsingError, 'Invalid data type*'):
            MessageParser.from_bytes(bytes([0xFF]))
