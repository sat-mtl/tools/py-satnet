from unittest import TestCase, mock

from satnet import SerializablePrefix
from satnet.client import Client
from satnet.message import Message, InternalMessageId
from satnet.request import Response
from satnet.serialization import Serializable
from .mock.base import MockSession, MockBaseAdapter
from .mock.client import MockClientAdapter
from .mock.commands import MockClientCommand, MockServerCommand
from .mock.request import MockClientRequest, MockClientResponse, MockServerRequest, MockServerResponse


class TestClient(TestCase):
    def setUp(self):
        self.adapter = MockBaseAdapter()
        # self.net = MockBase(adapter=self.adapter)
        self.session = MockSession()
        self.adapter = MockClientAdapter()

    def create_client(self):
        return Client(session=self.session, adapter=self.adapter)

    def test_initializes_not_connected(self):
        c = self.create_client()
        self.assertFalse(c.connected)

    def test_connect(self):
        c = self.create_client()
        with mock.patch.object(self.session, 'on_connecting') as on_con, \
                mock.patch.object(c, '_run') as c_run:
            c.connect("abc", 123)
            on_con.assert_called_once_with()
            self.assertEqual(self.adapter.host, "abc")
            self.assertEqual(self.adapter.port, 123)
            c_run.assert_called_once_with()

    def test_disconnect(self):
        c = self.create_client()
        with mock.patch.object(c, '_close') as c_close:
            c.disconnect()
            c_close.assert_called_once_with()

    def test_step_not_connected(self):
        c = self.create_client()
        with mock.patch.object(self.session, 'step') as d_step:
            c.step(123, 456)
            d_step.assert_not_called()

    def test_step_connected(self):
        c = self.create_client()
        c._connected = True
        now = 123
        dt = 456
        with mock.patch.object(self.session, 'step') as d_step:
            c.step(now, dt)
            d_step.assert_called_once_with(now=now, dt=dt)

    def test_on_message_not_message(self):
        client = self.create_client()
        with self.assertRaises(AssertionError):
            client._on_message("something else")

    def test_on_message_unhandled(self):
        client = self.create_client()
        data = Serializable()
        data.serialization_prefix = SerializablePrefix.CUSTOM
        message = Message(data=data)
        message.server_id = 0x00
        with mock.patch.object(client.session, 'on_message') as om:
            client._on_message(message)
            om.assert_called_once_with(message)

    def test_on_message_client_command(self):
        client = self.create_client()
        command = MockClientCommand()
        message = Message(command)
        with mock.patch.object(command, 'handle') as ch:
            self.assertRaises(AssertionError, client._on_message, message)
            ch.assert_not_called()

    def test_on_message_server_command(self):
        client = self.create_client()
        command = MockServerCommand()
        message = Message(command)
        with mock.patch.object(command, 'handle') as ch:
            client._on_message(message)
            ch.assert_called_once_with(session=self.session)

    def test_on_message_client_request(self):
        client = self.create_client()
        request = MockClientRequest()
        message = Message(request)
        with mock.patch.object(request, 'handle') as rh, \
                mock.patch.object(client, 'send') as rc_send:
            self.assertRaises(AssertionError, client._on_message, message)
            rh.assert_not_called()
            rc_send.assert_not_called()

    def test_on_message_server_request(self):
        client = self.create_client()
        request = MockServerRequest()
        message = Message(request)
        with mock.patch.object(request, 'handle', wraps=request.handle) as rh, \
                mock.patch.object(client, 'send') as rc_send:
            client._on_message(message)
            rh.assert_called_once_with(session=self.session, respond=mock.ANY)
            self.assertEqual(rc_send.call_count, 1)

            msg = rc_send.call_args[0][0]
            self.assertIsNotNone(msg)
            self.assertIsInstance(msg, Message)
            response = msg.data
            self.assertIsNotNone(response)
            self.assertIsInstance(response, Response)
            self.assertEqual(response, response)
            self.assertEqual(response.request_id, request.id)

    def test_on_message_client_response(self):
        client = self.create_client()

        # Fake that we have a pending request
        request = MockClientRequest()
        client._requests[0x01] = request

        response = MockClientResponse(request_id=0x01)
        message = Message(response)
        message.server_id = 0x00

        with mock.patch.object(response, 'handle') as rh:
            self.assertRaises(AssertionError, client._on_message, message)
            rh.assert_not_called()

    def test_on_message_server_response(self):
        client = self.create_client()

        # Fake that we have a pending request
        request = MockClientRequest()
        client._requests[0x01] = request

        response = MockServerResponse(request_id=0x01)
        message = Message(response)
        message.server_id = 0x00

        with mock.patch.object(response, 'handle') as rh:
            client._on_message(message)
            rh.assert_called_once_with(request=request, session=self.session)

    def test_on_internal_message_connected(self):
        client = self.create_client()
        with mock.patch.object(client.session, 'on_connected') as con:
            client._on_internal_message(InternalMessageId.CONNECTED, None)
            self.assertTrue(client.connected)
            con.assert_called_once_with()

    def test_on_internal_message_disconnected(self):
        client = self.create_client()
        with mock.patch.object(client.session, 'on_disconnected') as dis, \
                mock.patch.object(client, '_close_connection') as cc:
            client._on_internal_message(InternalMessageId.DISCONNECTED, None)
            self.assertFalse(client.connected)
            cc.assert_called_once_with()
            dis.assert_called_once_with()
