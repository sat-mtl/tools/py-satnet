from asynctest import CoroutineMock, MagicMock

from satnet.adapters.websockets.base import WSBase
from satnet.adapters.websockets.client import WSClient
from satnet.adapters.websockets.server import WSServer, WSConnection


class MockBase(WSBase):
    def _on_internal_message(self, message_id, payload):
        pass

    def run(self, now:float, dt:float):
        pass


class MockWSClient(WSClient):
    pass


class MockWSServer(WSServer):
    pass


class MockWSConnection(WSConnection):
    pass


class MagicMockContext(MagicMock):
    """
    Used to be able to patch a global scope method in a context manager ('with' block)
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        type(self).__aenter__ = CoroutineMock(return_value=MagicMock())
        type(self).__aexit__ = CoroutineMock(return_value=MagicMock())
