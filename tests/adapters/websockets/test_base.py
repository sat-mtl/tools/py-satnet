from unittest import mock

import aiounittest
import websockets
from asynctest import CoroutineMock

from satnet.message import Message
from .mock.websockets import MockBase


class TestWSBase(aiounittest.AsyncTestCase):
    def setUp(self):
        self.addCleanup(mock.patch.stopall)

    def test_run(self):
        base = MockBase()
        base.start()

    def test_close(self):
        base = MockBase()
        self.assertFalse(base._ev_loop.is_closed())
        base.stop()
        self.assertTrue(base._ev_loop.is_closed())

    async def test_consumer_ok_message(self):
        base = MockBase()
        socket = mock.Mock()

        socket.recv = CoroutineMock(return_value=b'\1good_message')
        ret = await base._consumer_handler(socket)
        socket.recv.assert_called_with()
        self.assertTrue(ret)
        self.assertEqual(len(base._network_incoming), 1)

        # Second message should add a new message in the incoming messages queue
        ret = await base._consumer_handler(socket)
        socket.recv.assert_called_with()
        self.assertTrue(ret)
        self.assertEqual(len(base._network_incoming), 2)
        base._ev_loop.stop()

    async def test_consumer_connection_closed(self):
        base = MockBase()
        socket = mock.Mock()

        # Returns false if connection lost when receiving from socket
        socket.recv = CoroutineMock(return_value=b'\1good_message',
                                    side_effect=websockets.ConnectionClosed(code=1, reason='Lost connection'))
        ret = await base._consumer_handler(socket)
        socket.recv.assert_called_with()
        self.assertFalse(ret)
        self.assertEqual(len(base._network_incoming), 0)
        socket.recv.side_effect = None

    async def test_consumer_invalid_type_msg(self):
        base = MockBase()
        socket = mock.Mock()

        socket.recv = CoroutineMock(return_value=b'\42invalid_type_msg')
        ret = await base._consumer_handler(socket)
        socket.recv.assert_called_with()
        self.assertTrue(ret)
        # No new message in queue because of invalid type
        self.assertEqual(len(base._network_incoming), 0)

    async def test_consumer_process_cb(self):
        base = MockBase()
        socket = mock.Mock()

        msg = []

        def cb(arg):
            msg.append('modified_arg')

        socket.recv = CoroutineMock(return_value=b'\1message_with_process_cb')
        ret = await base._consumer_handler(socket, cb)
        socket.recv.assert_called_with()
        self.assertTrue(ret)
        # No new message in queue because of invalid type
        self.assertEqual(len(base._network_incoming), 1)
        self.assertEqual(msg[0], 'modified_arg')

    async def test_producer_ok_message(self):
        base = MockBase()
        socket = mock.Mock()

        to_send = Message(raw=b'sent_msg')
        socket.send = CoroutineMock()
        base.outgoing_messages = mock.Mock(return_value=[to_send])
        ret = await base._producer_handler(socket)
        socket.send.assert_called_with(base._message_parser.to_bytes(to_send))
        self.assertTrue(ret)

    async def test_producer_connection_closed(self):
        base = MockBase()
        socket = mock.Mock()

        to_send = Message(raw=b'sent_msg')
        socket.send = CoroutineMock(side_effect=websockets.ConnectionClosed(code=1, reason='Lost connection'))
        base.outgoing_messages = mock.Mock(return_value=[to_send])
        ret = await base._producer_handler(socket)
        socket.send.assert_called_with(base._message_parser.to_bytes(to_send))
        self.assertFalse(ret)

    async def test_producer_invalid_message(self):
        base = MockBase()
        socket = mock.Mock()

        to_send = Message()
        socket.send = CoroutineMock()
        base.outgoing_messages = mock.Mock(return_value=[to_send])
        ret = await base._producer_handler(socket)
        socket.send.assert_not_called()
        self.assertTrue(ret)

