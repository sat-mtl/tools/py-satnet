from unittest import TestCase, mock

import zmq
from zmq import Context, Again

from satnet.errors import ParsingError
from satnet.message import Message, InternalMessageId
from satnet.adapters.zmq.base import ZMQFrameType, ZMQBase
from satnet.adapters.zmq.server import ZMQSession
from .mock.zmq import MockZMQSession, MockZMQServer
from ...mock.server import MockRemoteSession, MockServer
from ...mock.base import MockSession


class TestZMQRemoteClient(TestCase):

    def setUp(self):
        self.addCleanup(mock.patch.stopall)

    def get_session(self):
        self.server = MockZMQServer()
        self.server.heartbeat_interval = 1.00
        self.server.connection_timeout = 3.00
        self.server._socket = mock.Mock()
        return MockZMQSession(server=self.server, zmq_id=b'zmqid', envelope=[b'client', b'route'])

    def test_initialization(self):
        rc = self.get_session()
        self.assertEqual(rc.server, self.server)
        self.assertEqual(rc.zmq_id, b'zmqid')
        self.assertEqual(rc.envelope, [b'client', b'route'])

    # region step

    def test_step_init(self):
        rc = self.get_session()
        rc.step(1234, 0.5)
        self.assertEqual(rc._last_received_heartbeat, 1234)

    def test_step_nothing_to_do(self):
        rc = self.get_session()

        rc._last_received_heartbeat = 1233.5
        rc._last_sent_heartbeat = 1233.5

        with mock.patch.object(rc, 'disconnect') as disconnect:
            rc.step(now=1234, dt=0.5)
            disconnect.assert_not_called()
            self.server._socket.send_multipart.assert_not_called()

    def test_step_timeout(self):
        rc = self.get_session()

        rc._last_received_heartbeat = 1231
        rc._last_sent_heartbeat = 1233.5

        with mock.patch.object(rc, 'disconnect') as disconnect:
            rc.step(now=1234, dt=0.5)
            disconnect.assert_called_once_with()
            self.server._socket.send_multipart.assert_not_called()

    def test_step_heartbeat(self):
        rc = self.get_session()

        rc._last_received_heartbeat = 1233.5
        rc._last_sent_heartbeat = 1233

        with mock.patch.object(rc, 'disconnect') as disconnect:
            rc.step(now=1234, dt=0.5)
            disconnect.assert_not_called()
            self.server._socket.send_multipart.assert_called_once_with((b'client', b'route', bytes([ZMQFrameType.HEARTBEAT.value])),zmq.DONTWAIT)

    def test_step_heartbeat(self):
        rc = self.get_session()
        self.server._socket.send_multipart.side_effect = Again

        rc._last_received_heartbeat = 1233.5
        rc._last_sent_heartbeat = 1233

        with mock.patch.object(rc, 'disconnect') as disconnect:
            rc.step(now=1234, dt=0.5)
            self.server._socket.send_multipart.assert_called_once_with((b'client', b'route', bytes([ZMQFrameType.HEARTBEAT.value])), zmq.DONTWAIT)
            disconnect.assert_called_once_with()

    # endregion

    def test_heartbeat_in(self):
        rc = self.get_session()
        time = mock.patch('time.time', return_value=1234).start()
        rc.heartbeat_in()
        self.assertEqual(rc._last_received_heartbeat, 1234)

    def test_heartbeat_out(self):
        rc = self.get_session()
        time = mock.patch('time.time', return_value=1234).start()
        rc.heartbeat_out()
        self.assertEqual(rc._last_sent_heartbeat, 1234)

    def test_disconnect(self):
        rc = self.get_session()
        with mock.patch.object(self.server, 'disconnect') as disconnect:
            rc.disconnect()
            disconnect.assert_called_once_with(rc)


class TestZMQServer(TestCase):

    def setUp(self):
        self.addCleanup(mock.patch.stopall)

    def test_get_socket(self):
        server = MockZMQServer()
        server._port = 1234
        socket = mock.Mock()
        context = mock.Mock()
        context.socket.return_value = socket
        s = server._get_socket(context)
        self.assertEqual(s, socket)
        context.socket.assert_called_once_with(zmq.ROUTER)
        socket.setsockopt.assert_called_with(zmq.RCVHWM, 0)
        socket.bind.assert_called_once_with("tcp://*:1234")

    # region wrap

    def test_wrap_no_client(self):
        server = MockZMQServer()
        message = Message()
        data = server._wrap(message)
        self.assertIsNone(data)

    def test_wrap_client_not_found(self):
        mp = mock.Mock()
        mp.to_bytes.return_value = b'test'
        server = MockZMQServer(message_parser=mp)

        session = MockSession()
        client = MockRemoteSession(server=MockServer(adapter=server), id=0xFF)
        zmq_client = MockZMQSession(server=server, zmq_id=b'zmqid', envelope=[b'client', b'route'])

        message = Message(session=client)
        with mock.patch.object(zmq_client, 'heartbeat_out') as hb:
            data = server._wrap(message)
            self.assertIsNone(data)
            hb.assert_not_called()
            mp.to_bytes.assert_not_called()

    def test_wrap(self):
        mp = mock.Mock()
        mp.to_bytes.return_value = b'test'
        server = MockZMQServer(message_parser=mp)

        session = MockSession()
        client = MockRemoteSession(server=MockServer(adapter=server), id=0xFF)
        zmq_client = MockZMQSession(server=server, zmq_id=b'zmqid', envelope=[b'client', b'route'])
        server._session_adapters[client.id] = zmq_client

        message = Message(session=client)
        with mock.patch.object(zmq_client, 'heartbeat_out') as hb:
            data = server._wrap(message)
            self.assertEqual(data, (b'client', b'route', bytes([ZMQFrameType.CUSTOM.value]) + b'test'))
            hb.assert_called_with()
            mp.to_bytes.assert_called_with(message)

    # endregion

    # region unwrap

    def test_unwrap_custom_creates_client(self):
        mp = mock.Mock()
        message = Message()
        mp.from_bytes.return_value = message
        server = MockZMQServer(message_parser=mp)

        self.assertEqual(len(server._zmq_sessions), 0)
        self.assertEqual(len(server._session_adapters), 0)

        # We have to patch the class since the instance is not created yet
        with mock.patch.object(ZMQSession, 'heartbeat_in') as hb:
            server._unwrap([b'client', b'route', bytes([ZMQFrameType.CUSTOM.value]) + b'test'])
            hb.assert_called_once_with()

        self.assertEqual(len(server._zmq_sessions), 1)
        self.assertIsInstance(server._zmq_sessions.get(b'clientroute'), ZMQSession)
        self.assertEqual(len(server._session_adapters), 1)
        self.assertIsInstance(server._session_adapters.get(ZMQSession._id_counter), ZMQSession)
        self.assertEqual(server._internal_incoming.popleft(),
                         (InternalMessageId.SESSION_CONNECTED, server._session_adapters.get(ZMQSession._id_counter)))
        mp.from_bytes.assert_called_once_with(bytes([ZMQFrameType.CUSTOM.value]) + b'test', 1)
        self.assertEqual(message.session_id, ZMQSession._id_counter)
        self.assertEqual(len(server._network_incoming), 1)
        self.assertEqual(server._network_incoming.popleft(), message)

    def test_unwrap_custom_existing_client(self):
        mp = mock.Mock()
        message = Message()
        mp.from_bytes.return_value = message
        server = MockZMQServer(message_parser=mp)

        session = MockSession()
        zmq_client = MockZMQSession(server=server, zmq_id=b'zmqid', envelope=[b'client', b'route'])
        client = MockRemoteSession(server=MockServer(adapter=server), id=zmq_client.id)
        server._zmq_sessions[b'clientroute'] = zmq_client
        server._session_adapters[client.id] = zmq_client

        self.assertEqual(len(server._zmq_sessions), 1)
        self.assertEqual(len(server._session_adapters), 1)

        # We have to patch the class since the instance is not created yet
        with mock.patch.object(ZMQSession, 'heartbeat_in') as hb:
            server._unwrap([b'client', b'route', bytes([ZMQFrameType.CUSTOM.value]) + b'test'])
            hb.assert_called_once_with()

        self.assertEqual(len(server._zmq_sessions), 1)  # Just to make sure it didn't try to add a new one
        self.assertEqual(len(server._session_adapters), 1)  # Just to make sure it didn't try to add a new one
        self.assertEqual(len(server._internal_incoming), 0)
        mp.from_bytes.assert_called_once_with(bytes([ZMQFrameType.CUSTOM.value]) + b'test', 1)
        self.assertEqual(message.session_id, client.id)
        self.assertEqual(len(server._network_incoming), 1)
        self.assertEqual(server._network_incoming.popleft(), message)

    def test_unwrap_custom_failure(self):
        mp = mock.Mock()
        mp.from_bytes.side_effect = ParsingError
        server = MockZMQServer(message_parser=mp)

        # We have to patch the class since the instance is not created yet
        with mock.patch.object(ZMQSession, 'heartbeat_in') as hb:
            server._unwrap([b'client', b'route', bytes([ZMQFrameType.CUSTOM.value]) + b'test'])
            hb.assert_called_once_with()

        mp.from_bytes.assert_called_once_with(bytes([ZMQFrameType.CUSTOM.value]) + b'test', 1)
        self.assertEqual(len(server._network_incoming), 0)

    def test_unwrap_heartbeat(self):
        mp = mock.Mock()
        server = MockZMQServer(message_parser=mp)

        # We have to patch the class since the instance is not created yet
        with mock.patch.object(ZMQSession, 'heartbeat_in') as hb:
            server._unwrap([b'client', b'route', bytes([ZMQFrameType.HEARTBEAT.value]) + b'test'])
            hb.assert_called_once_with()

        mp.from_bytes.assert_not_called()
        self.assertEqual(len(server._network_incoming), 0)

    # endregion

    def test_step(self):
        server = MockZMQServer()
        zmq_client = MockZMQSession(server=server, zmq_id=b'zmqid', envelope=[b'client', b'route'])
        server._session_adapters[0xFF] = zmq_client
        with mock.patch.object(zmq_client, 'step') as step:
            server._run(123, 456)
            step.assert_called_once_with(now=123, dt=456)

    def test_disconnect(self):
        server = MockZMQServer()
        server._socket = mock.Mock()
        zmq_client = MockZMQSession(server=server, zmq_id=b'zmqid', envelope=[b'client', b'route'])
        server._zmq_sessions[b'zmqid'] = zmq_client
        server._session_adapters[zmq_client.id] = zmq_client
        server.disconnect(zmq_client)
        self.assertIsNone(server._zmq_sessions.get(b'zmqid'))
        self.assertIsNone(server._session_adapters.get(zmq_client.id))
        self.assertEqual(server._internal_incoming.popleft(), (InternalMessageId.SESSION_DISCONNECTED, zmq_client))
        server._socket.send_multipart((*zmq_client.envelope, bytes([ZMQFrameType.CONNECTION_CLOSED.value])), zmq.DONTWAIT)

    def test_disconnect_ignore_key_error(self):
        server = MockZMQServer()
        server._socket = mock.Mock()
        zmq_client = MockZMQSession(server=server, zmq_id=b'zmqid', envelope=[b'client', b'route'])
        server.disconnect(zmq_client)
        self.assertEqual(len(server._internal_incoming), 0)
        server._socket.send_multipart.assert_not_called()

    def test_on_internal_message_disconnect(self):
        client = mock.Mock()
        server = MockZMQServer()
        server._session_adapters[0xFF] = client
        server._on_internal_message(message_id=InternalMessageId.SESSION_DISCONNECT, payload=0xFF)
        client.disconnect.assert_called_once_with()
