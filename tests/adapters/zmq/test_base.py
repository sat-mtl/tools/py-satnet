from unittest import TestCase, mock

import zmq
from zmq import Context, Again

from satnet.message import Message
from .mock.zmq import MockBase


class TestZMQBase(TestCase):

    def setUp(self):
        self.addCleanup(mock.patch.stopall)

    def test_close(self):
        base = MockBase()
        self.assertFalse(base._abort)
        base.stop()
        self.assertTrue(base._abort)

    def test_run(self):
        base = MockBase()
        base._abort = True  # We don't want to enter the loop

        context = mock.Mock()
        context_instance = mock.patch.object(Context, 'instance', return_value=context).start()

        socket = mock.Mock()
        base_get_context = mock.patch.object(base, '_get_socket', return_value=socket).start()

        base.start()

        context_instance.assert_called_once_with()
        base_get_context.assert_called_once_with(context)
        self.assertEqual(base._socket, socket)
        socket.close.assert_called_once_with()

    def make_runnable_base(self):
        self.base = MockBase()
        self.base._socket = mock.Mock()
        self.oim = mock.patch.object(self.base, '_on_internal_message').start()
        self.unwrap = mock.patch.object(self.base, '_unwrap').start()
        self.wrap = mock.patch.object(self.base, '_wrap').start()
        self._run = mock.patch.object(self.base, '_run').start()

    def test_run_delta_t(self):
        self.make_runnable_base()
        self.base.run(now=2.0, dt=0.1)
        self._run.assert_called_once_with(now=2.0, dt=0.1)

    def test_run_nothing_to_do(self):
        self.make_runnable_base()
        self.base._socket.recv_multipart.side_effect = Again
        self.base.run(now=2.0, dt=0.1)
        self.oim.assert_not_called()
        self.unwrap.assert_not_called()
        self.base._socket.recv_multipart.assert_called_once_with(zmq.NOBLOCK)
        self.wrap.assert_not_called()

    def test_run_internal_message(self):
        self.make_runnable_base()
        self.base._socket.recv_multipart.side_effect = Again
        self.base._internal_outgoing.append((0xFF, "Something"))
        self.base.run(now=2.0, dt=0.1)
        self.oim.assert_called_once_with(message_id=0xFF, payload="Something")

    def test_run_incoming_message(self):
        self.make_runnable_base()
        self.base._socket.recv_multipart.side_effect = [bytes([0xFF]), Again]
        self.base.run(now=2.0, dt=0.1)
        self.assertEqual(self.base._socket.recv_multipart.call_count, 2)
        self.unwrap.assert_called_once_with(bytes([0xFF]))

    def test_run_incoming_message_batch_limit(self):
        self.make_runnable_base()
        self.base.batch_size = 10
        self.base._socket.recv_multipart.side_effect = [*[bytes([x]) for x in range(100)], Again]
        self.base.run(now=2.0, dt=0.1)
        self.assertEqual(self.base._socket.recv_multipart.call_count, 10)
        self.assertEqual(self.unwrap.call_count, 10)
        for i in range(len(self.unwrap.call_args_list)):
            self.assertEqual(self.unwrap.call_args_list[i][0][0], bytes([i]))

    def test_run_outgoing_message(self):
        self.make_runnable_base()
        message = Message()
        self.base._network_outgoing.append(message)
        self.wrap.return_value = [bytes([0xFF])]
        self.base.run(now=2.0, dt=0.1)
        self.wrap.assert_called_once_with(message)
        # self.base._socket.send_multipart.assert_called_once_with([bytes([0xFF])], zmq.DONTWAIT, copy=False)
        self.base._socket.send_multipart.assert_called_once_with([bytes([0xFF])], zmq.DONTWAIT)

    def test_run_outgoing_message_empty(self):
        self.make_runnable_base()
        message = Message()
        self.base._network_outgoing.append(message)
        self.wrap.return_value = None
        self.base.run(now=2.0, dt=0.1)
        self.wrap.assert_called_once_with(message)
        self.base._socket.send_multipart.assert_not_called()

    def test_run_outgoing_message_batch_limit(self):
        self.make_runnable_base()
        self.base.batch_size = 10
        messages = [Message() for i in range(10)]
        self.base._network_outgoing.extend(messages)
        self.wrap.side_effect = [[bytes([i])] for i in range(10)]
        self.base.run(now=2.0, dt=0.1)
        self.assertEqual(self.wrap.call_count, 10)
        self.assertEqual(self.base._socket.send_multipart.call_count, 10)
        for i in range(len(self.unwrap.call_args_list)):
            self.assertEqual(self.wrap.call_args_list[i][0][0], messages[i])
            self.assertEqual(self.base._socket.send_multipart.call_args_list[i][0][0], [bytes([i])])

    def test_run_outgoing_message_full_queue(self):
        """ For now we close the connection on a full queue """
        self.make_runnable_base()
        self.base._socket.send_multipart.side_effect = Again
        message = Message()
        self.base._network_outgoing.append(message)
        self.wrap.return_value = [bytes([0xFF])]
        with mock.patch.object(self.base, 'stop') as close:
            self.base.run(now=2.0, dt=0.1)
            self.wrap.assert_called_once_with(message)
            # self.base._socket.send_multipart.assert_called_once_with([bytes([0xFF])], zmq.DONTWAIT, copy=False)
            self.base._socket.send_multipart.assert_called_once_with([bytes([0xFF])], zmq.DONTWAIT)
            close.assert_called_once_with()
