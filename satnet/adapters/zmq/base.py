import logging
import time
from abc import ABCMeta, abstractmethod
from enum import IntEnum, unique
from time import sleep
from typing import Any, Dict, List, Optional, TYPE_CHECKING, Tuple

import zmq  # type: ignore
from zmq import Again, Context, Socket

from satnet.base import BaseAdapter, defaultAdapterConfig

if TYPE_CHECKING:
    from satnet.message import Message, MessageParser

logger = logging.getLogger(__name__)

defaultAdapterConfig['zmq.batch_size'] = 1024
defaultAdapterConfig['zmq.heartbeat_interval'] = 1.00
defaultAdapterConfig['zmq.connection_timeout'] = 5.00


@unique
class ZMQFrameType(IntEnum):
    HEARTBEAT = 0x01
    CONNECTION_CLOSED = 0x02
    CUSTOM = 0x03


class ZMQBase(BaseAdapter, metaclass=ABCMeta):
    """
    Base ZMQ Network Adapter
    This provides abstract methods and shared logic for zmq network adapter
    """

    def __init__(
            self,
            config: Optional[Dict[str, Any]] = None,
            message_parser: Optional['MessageParser'] = None,
    ) -> None:
        super().__init__(config=config, message_parser=message_parser)
        self._abort = False
        self._last_loop = 0.00
        self._socket = None  # type: Socket
        self.batch_size: int = int(self._config.get('zmq.batch_size'))
        self.heartbeat_interval: float = float(self._config.get('zmq.heartbeat_interval'))
        self.connection_timeout: float = float(self._config.get('zmq.connection_timeout'))

    def stop(self) -> None:
        super().stop()
        logger.info("Setting abort flag...")
        self._abort = True

    # region Net Thread

    @property
    def socket(self) -> Socket:
        """
        ZMQ Socket instance
        :return: Socket
        """
        return self._socket

    def start(self) -> None:
        super().start()
        logger.info("Initializing...")

        context = Context.instance()
        self._socket = self._get_socket(context)
        self._last_loop = time.time()

        logger.info("Running!")

        if self._use_threads:
            while not self._abort:
                now = time.time()
                dt = now - self._last_loop
                self._last_loop = now
                self.run(now=now, dt=dt)
                sleep(0.001)
            self._socket.close()
            logger.info("Thread exited!")

    def run(self, now: float, dt: float) -> None:
        """
        Private run loop method, separated from `run() for testing purposes.
        :return: None
        """

        # Check internal messages
        for message_id, payload in self.outgoing_internal_messages():
            self._on_internal_message(message_id=message_id, payload=payload)

        count = 0
        receiving = True
        while (self.batch_size == 0 or count < self.batch_size) and receiving:
            try:
                # TODO: Do non-copying receive also
                self._unwrap(self._socket.recv_multipart(zmq.NOBLOCK))
            except Again:
                receiving = False
            else:
                count += 1

        self._run(now=now, dt=dt)

        # Send multipart messages
        count = 0
        for message in self.outgoing_messages():
            frames = self._wrap(message)

            if frames is not None:
                try:
                    # self._socket.send_multipart(frames, zmq.DONTWAIT, copy=True)
                    self._socket.send_multipart(frames, zmq.DONTWAIT)
                except Again:
                    logger.error("ZMQ send queue is full!")
                    # For now we close the connection on a full queue
                    # until we figure out a way to handle these situations
                    self.stop()
                    break
                else:
                    count += 1
            else:
                logger.warning("Message did not generate frames.")

            if 0 < self.batch_size <= count:
                break

    @abstractmethod
    def _get_socket(self, context: Context) -> Socket:
        """`
        Abstract socket creation method.
        This method should be implemented in order to return the socket to be used

        :param context: Context
        :return: Socket
        """
        raise NotImplementedError('users must define `_get_socket(self, context: Context) -> Socket` to use this base class')

    @abstractmethod
    def _wrap(self, message: 'Message') -> Optional[Tuple[bytes, ...]]:
        """
        Wrap the Message into raw zmq frames tuple

        :param message: Message
        :return: Optional[Tuple[bytes, ...]]
        """
        raise NotImplementedError('users must define `def _wrap(self, message: Message) -> Optional[Tuple[bytes, ...]]` to use this base class')

    @abstractmethod
    def _unwrap(self, msg: List[bytes]) -> None:
        """
        Unwrap the raw zmq frames tuple into a Message and handle accordingly

        :param msg: List[bytes]
        :return: None
        """
        raise NotImplementedError('users must define `def _unwrap(self, msg: tuple) -> Message` to use this base class')

    @abstractmethod
    def _run(self, now: float, dt: float) -> None:
        """
        Internal run method
        :return: None
        """

    # endregion
    ...
