import logging
import time
from typing import Any, Dict, List, Optional, TYPE_CHECKING, Tuple, cast

import zmq  # type: ignore
from zmq import Again, Context, Socket

from satlib.utils import debug_utils
from satnet import HEX_DUMP
from satnet.errors import ParsingError
from satnet.message import InternalMessageId
from satnet.server import ServerAdapter, SessionAdapter
from .base import ZMQBase, ZMQFrameType

if TYPE_CHECKING:
    from satnet.message import Message, MessageParser

logger = logging.getLogger(__name__)


class ZMQSession(SessionAdapter):
    """
    ZMQ Remote Session Adapter
    """

    def __init__(self, server: 'ZMQServer', zmq_id: bytes, envelope: List[bytes]) -> None:
        """
        Create a zmq server remote session
        See `SessionAdapter` for more information about arguments.

        The envelope is a list of ZMQ frames that precede any message received from a client.
        It is kept in order to be able to address that client back when sending messages.

        :param server: ZMQServer
        :param envelope: List[bytes]
        """
        super().__init__(server=server)
        self._zmq_id = zmq_id
        self._envelope = envelope
        self._last_received_heartbeat = 0.00
        self._last_sent_heartbeat = 0.00

    # region Properties

    @property
    def server(self) -> 'ZMQServer':
        """
        "Casted" server instance

        :return: ZMQServer
        """
        return cast(ZMQServer, self._server)

    @property
    def zmq_id(self) -> bytes:
        """
        Bytes id generated from the routing frames in the zmq message

        :return: bytes
        """
        return self._zmq_id

    @property
    def envelope(self) -> List[bytes]:
        """
        Client addressing envelope

        :return: List[bytes]
        """
        return self._envelope

    # endregion

    # region Lifecycle

    def step(self, now: float, dt: float) -> None:
        if self._last_received_heartbeat == 0.00:
            # Initialize heartbeat, otherwise it will start and consider the connection already expired
            # since we'll always be comparing against a last received heartbeat from 1970 ;)
            self._last_received_heartbeat = now

        elif now - self._last_received_heartbeat >= self.server.connection_timeout:
            # Check for disconnection
            logger.warning("Disconnecting client id: \"{}\" due to inactivity".format(self._id))
            self.disconnect()

        elif now - self._last_sent_heartbeat >= self.server.heartbeat_interval:
            # Send heartbeats
            logger.debug("Heartbeat sent from client \"{}\"".format(self._id))
            try:
                self.server.socket.send_multipart(
                    (*self._envelope, bytes([ZMQFrameType.HEARTBEAT.value])),
                    zmq.DONTWAIT
                )
            except Again:
                logger.error("ZMQ send queue is full!")
                self.disconnect()
            self._last_sent_heartbeat = now

    # endregion

    # region Methods

    def heartbeat_in(self) -> None:
        """
        Record an incoming heartbeat time

        :return:
        """
        self._last_received_heartbeat = time.time()

    def heartbeat_out(self) -> None:
        """
        Record an outgoing heartbeat time

        :return:
        """
        self._last_sent_heartbeat = time.time()

    def disconnect(self) -> None:
        """
        Disconnect this session

        :return: None
        """
        self.server.disconnect(self)

    # endregion


class ZMQServer(ZMQBase, ServerAdapter[ZMQSession]):
    """
    ZMQ Server Network Adapter
    """

    def __init__(
            self,
            message_parser: Optional['MessageParser'] = None,
            config: Optional[Dict[str, Any]] = None,
    ) -> None:
        super().__init__(message_parser=message_parser, config=config)
        self._zmq_sessions = {}  # type: Dict[bytes, ZMQSession]

    # region Net Thread

    def _get_socket(self, context: Context) -> Socket:
        socket = context.socket(zmq.ROUTER)
        socket.setsockopt(zmq.LINGER, 0)
        socket.setsockopt(zmq.SNDHWM, 0)
        socket.setsockopt(zmq.RCVHWM, 0)
        socket.bind("tcp://*:{}".format(self._port))
        return socket

    def _wrap(self, message: 'Message') -> Optional[Tuple[bytes, ...]]:
        # Using a ROUTER socket requires us to start with routing frames
        # We keep the routing information in the ZMQSession instance associated with the client's connection

        if not message.session:
            logger.warning("Trying to send a message without a recipient")
            return None

        session = self._session_adapters.get(message.session.id)
        if not session:
            logger.warning("Trying to send a message to a non-existing session {}".format(message.session))
            return None

        # Record outgoing heartbeat
        session.heartbeat_out()

        data = bytes([ZMQFrameType.CUSTOM.value]) + self._message_parser.to_bytes(message)

        logger.debug("Sending message")
        if HEX_DUMP:
            logger.debug(debug_utils.bytes_to_hex(data))

        # noinspection PyRedundantParentheses
        return (*session.envelope, data)

    def _unwrap(self, msg: List[bytes]) -> None:
        # The envelope (zmq routing) consists of everything but the last frame
        # It will include an empty separator frame but it is ok since we also have to include it
        # when sending a message to a client
        envelope = msg[:-1]

        # Our protocol data is contained in the last frame
        data = msg[-1]

        # Concatenate all envelope frames in order to generate a unique client id
        # Otherwise, just using the last element would result in conflicts if the
        # outer network layer were to reuse ids (which it does)
        session_id = b''.join(envelope)

        # Get session
        session = self._zmq_sessions.get(session_id)
        if not session:
            # TODO: Move out of `_unwrap`, it is lower level than message unwrapping
            session = ZMQSession(server=self, zmq_id=session_id, envelope=envelope)
            self._session_adapters[session.id] = session
            self._zmq_sessions[session_id] = session
            self._internal_incoming.append((InternalMessageId.SESSION_CONNECTED, session))

        # Record incoming heartbeat
        session.heartbeat_in()

        # Header
        msg_type = ZMQFrameType(int(data[0]))

        # Handle depending on type
        if msg_type is ZMQFrameType.CUSTOM:
            # Place in incoming message queue
            logger.debug("Received message")
            if HEX_DUMP:
                logger.debug(debug_utils.bytes_to_hex(data))
            try:
                message = self._message_parser.from_bytes(data, 1)
            except ParsingError as e:
                logger.error(str(e))
                return
            message.session_id = session.id
            self._network_incoming.append(message)

        elif msg_type is ZMQFrameType.HEARTBEAT:
            logger.debug("Heartbeat received from client {}".format(session.id))

        else:
            logger.warning("Unknown ZMQ message type \"{}\"".format(msg_type))

    def _run(self, now: float, dt: float) -> None:
        # TODO: Find a way to remove disconnected sessions that won't require copying the list for iterating
        for session in list(self._session_adapters.values()):
            session.step(now=now, dt=dt)

    def disconnect(self, session: ZMQSession) -> None:
        """
        Disconnect a session

        :param session: ZMQSession - Session
        :return: None
        """
        try:
            del self._session_adapters[session.id]
            del self._zmq_sessions[session.zmq_id]
        except KeyError:
            logger.error("Client was not found for disconnection")
        else:
            self._internal_incoming.append((InternalMessageId.SESSION_DISCONNECTED, session))
            self.socket.send_multipart((*session.envelope, bytes([ZMQFrameType.CONNECTION_CLOSED.value])), zmq.DONTWAIT)

    def _on_internal_message(self, message_id: InternalMessageId, payload: Any) -> None:
        if message_id is InternalMessageId.SESSION_DISCONNECT:
            session_id = payload  # type: int
            logger.info("Disconnecting client {}".format(session_id))
            session = self._session_adapters.get(session_id)
            if session:
                session.disconnect()
            else:
                logger.warning("Client {} does not exist".format(session_id))

    # endregion
