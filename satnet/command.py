from abc import abstractmethod
from enum import IntEnum, unique
from typing import Callable, Optional, List, TypeVar, Generic, TYPE_CHECKING

from satnet import SerializablePrefix
from satnet.serialization import Serializable, serializable

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from satnet.session import Session
    # noinspection PyUnresolvedReferences
    from satnet.client import LocalSession
    # noinspection PyUnresolvedReferences
    from satnet.server import RemoteSession


@unique
class CommandId(IntEnum):
    """
    EIS Command Ids
    """
    SUBSCRIBE = 0x01
    UNSUBSCRIBE = 0x02
    UNDO = 0x03
    REDO = 0x04


S = TypeVar('S', bound='Session')
RS = TypeVar('RS', bound='RemoteSession')
LS = TypeVar('LS', bound='LocalSession')


def builtin_command(id: int, fields: Optional[List[str]] = None) -> Callable:
    """
    Helper decorator just for semantics and also adding the prefix ;)
    This is just wrapping the serializable decorator
    """
    return serializable(SerializablePrefix.BUILTIN_COMMAND, id, fields)


def command(id: int, fields: Optional[List[str]] = None) -> Callable:
    """
    Helper decorator just for semantics and also adding the prefix ;)
    This is just wrapping the serializable decorator
    """
    return serializable(SerializablePrefix.COMMAND, id, fields)


class Command(Generic[S], Serializable):
    """
    Command class
    """

    def __init__(self) -> None:
        """
        Create a command object
        """
        super().__init__()

    def __str__(self) -> str:
        return self.__class__.__name__

    def on_sending(self, session: S) -> None:
        """
        Called when the command is about to be queued to be sent on the network

        :param session: S
        :return: None
        """

    @abstractmethod
    def handle(self, session: S) -> None:
        """
        Handle the command

        :param session: T - Session instance on which to run the command
        :return: None
        """
        raise NotImplementedError('users must define `def handle(self, session: S) -> None` to use this base class')


class ClientCommand(Generic[RS], Command[RS]):
    pass


class ServerCommand(Generic[LS], Command[LS]):
    pass
