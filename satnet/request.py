from abc import abstractmethod
from typing import TypeVar, Generic, Callable, Optional, List, TYPE_CHECKING
from satnet import SerializablePrefix
from satnet.serialization import Serializable, serializable

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from satnet.server import RemoteSession
    # noinspection PyUnresolvedReferences
    from satnet.client import LocalSession
    # noinspection PyUnresolvedReferences
    from satnet.session import Session


S = TypeVar('S', bound='Session')
RS = TypeVar('RS', bound='RemoteSession')
LS = TypeVar('LS', bound='LocalSession')

R = TypeVar('R', bound='Request')
CR = TypeVar('CR', bound='ClientRequest')
SR = TypeVar('SR', bound='ServerRequest')

ResponseCallback = Callable[['Response'], None]


def request(id: int, fields: Optional[List[str]] = None) -> Callable:
    """
    Helper decorator just for semantics and also adding the prefix ;)
    This is just wrapping the serializable decorator
    """
    return serializable(SerializablePrefix.REQUEST, id, fields)


def response(id: int, fields: Optional[List[str]] = None) -> Callable:
    """
    Helper decorator just for semantics and also adding the prefix ;)
    This is just wrapping the serializable decorator
    """
    return serializable(SerializablePrefix.RESPONSE, id, fields)


class Response(Generic[S, R], Serializable):
    """
    Response class
    """

    _fields = ['request_id']

    def __init__(self, request_id: Optional[int] = None) -> None:
        """
        Create a response object
        :param request_id: int - A request id is needed for the requester to know what request generated the response
        """
        super().__init__()
        self.request_id = request_id

    def __str__(self) -> str:
        return self.__class__.__name__

    def on_sending(self, session: S) -> None:
        """
        Called when the response is about to be queued to be sent on the network

        :param session: S
        :return: None
        """

    @abstractmethod
    def handle(self, request: R, session: S) -> None:
        """
        Handle the response

        This method will be called by the party receiving the response in order to handle it.
        The session argument will be the local/remote session <T> that sent the request, depending
        on if the requestor is local or remote.

        This method should return a response that will be sent to the requestor.

        :param request: R - Original request
        :param session: S - Session instance on which to apply the response
        :return: None
        """
        raise NotImplementedError('users must define `def handle(self, request: R, session: S) -> None` to use this base class')


class ClientResponse(Generic[RS, SR], Response[RS, SR]):
    pass


class ServerResponse(Generic[LS, CR], Response[LS, CR]):
    pass


class Request(Generic[S], Serializable):
    """
    Request base class
    This class should neither be subclassed nor used directly
    as it is the base for the two distinct types of requests defined later.
    """

    _id_counter = -1
    _fields = ['_id']

    @classmethod
    def get_id(cls) -> int:
        """
        Generate a new pseudo unique request id

        :return: int
        """
        cls._id_counter += 1
        return cls._id_counter

    def __init__(self) -> None:
        super().__init__()
        self._id: int = Request.get_id()

    def __str__(self) -> str:
        return self.__class__.__name__

    @property
    def id(self) -> int:
        """
        Request id

        :return: int
        """
        return self._id

    def on_sending(self, session: S) -> None:
        """
        Called when the request is about to be queued to be sent on the network

        :param session: S
        :return: None
        """

    @abstractmethod
    def handle(self, session: S, respond: ResponseCallback) -> None:
        """
        Handle the request

        This method will be called by the party receiving the request in order to handle it.
        The session argument will be the local/remote session <T> that sent the request, depending
        on if the requestor is local or remote.

        This method should call the respond callback with a response that will be sent to the requestor.

        :param session: S - Session instance on which to run the request
        :param respond: Callable[[Response], None]
        :return: None
        """


class ClientRequest(Generic[RS], Request[RS]):
    pass


class ServerRequest(Generic[LS], Request[LS]):
    pass
