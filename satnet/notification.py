from abc import abstractmethod
from typing import Callable, Optional, List, Generic, TypeVar, TYPE_CHECKING
from uuid import UUID

from satnet import SerializablePrefix
from satnet.serialization import Serializable, serializable

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from satnet.server import RemoteSession
    # noinspection PyUnresolvedReferences
    from satnet.client import LocalSession
    # noinspection PyUnresolvedReferences
    from satnet.session import Session

S = TypeVar('S', bound='Session')
RS = TypeVar('RS', bound='RemoteSession')
LS = TypeVar('LS', bound='LocalSession')


def notification(id: int, fields: Optional[List[str]] = None) -> Callable:
    """
    Helper decorator just for semantics and also adding the prefix ;)
    This is just wrapping the serializable decorator
    """
    return serializable(SerializablePrefix.NOTIFICATION, id, fields)


class Notification(Generic[S], Serializable):
    """
    Notification class
    """

    _fields = ['_context']

    def __init__(self, context: Optional[UUID] = None) -> None:
        """
        Create a notification object
        """
        super().__init__()
        self._context = context

    def __str__(self) -> str:
        return self.__class__.__name__

    @property
    def context(self) -> Optional[UUID]:
        return self._context

    @property
    def topic(self) -> int:
        """
        For now, just reuse the serialization id as the topic since it should already come from a notification enum

        :return: int
        """
        return self.serialization_id

    def on_sending(self, session: S) -> None:
        """
        Called when the notification is about to be queued to be sent on the network

        :param session: S
        :return: None
        """

    @abstractmethod
    def handle(self, session: S) -> None:
        """
        Handle the notification on the client
        This code is executed on the client handling the notification

        :param session: S - Session instance on which to run the notification
        :return: None
        """
        raise NotImplementedError('users must define `def handle(self, session: S) -> None` to use this base class')


class ClientNotification(Generic[RS], Notification[RS]):
    pass


class ServerNotification(Generic[LS], Notification[LS]):
    pass
