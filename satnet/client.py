import logging
from abc import ABCMeta
from typing import Any, Dict, Optional, TypeVar, Generic, TYPE_CHECKING

from satnet.action import Action
from satnet.base import NetBase, BaseAdapter
from satnet.command import ClientCommand, Command
from satnet.message import InternalMessageId, Message
from satnet.notification import ServerNotification
from satnet.request import Response, ClientResponse, ServerRequest, ServerResponse
from satnet.session import Session
from satnet import SerializablePrefix


if TYPE_CHECKING:
    from .message import MessageParser

logger = logging.getLogger(__name__)


class ClientAdapter(BaseAdapter, metaclass=ABCMeta):
    """
    Base Client Network Adapter
    This provides abstract methods and shared logic for client network adapters
    """

    def __init__(
            self, message_parser: Optional['MessageParser'] = None,
            config: Optional[Dict[str, Any]] = None,
    ) -> None:
        super().__init__(message_parser=message_parser, config=config)
        self._host: str = self._config.get('host')
        self._port: int = self._config.get('port')

    @property
    def host(self) -> str:
        """
        Host to connect to
        :return: str
        """
        return self._host

    @host.setter
    def host(self, value: str) -> None:
        self._host = value

    @property
    def port(self) -> int:
        """
        Port to connect to on the host
        :return: int
        """
        return self._port

    @port.setter
    def port(self, value: int) -> None:
        self._port = value


class LocalSession(Session, metaclass=ABCMeta):
    """
    Class representing a client connected to a server.
    It hold anything related to the client-side state of that client and
    provides helper methods to directly communicate with the server.
    """

    def __init__(
            self,
            client: 'Client'
    ) -> None:
        assert isinstance(client, Client)
        super().__init__(net=client)

    @property
    def client(self) -> 'Client':
        assert isinstance(self._net, Client)
        return self._net

    def disconnect(self) -> None:
        """
        Disconnect this session

        :return: None
        """
        self.client.disconnect()

    def action(self, action: Action) -> None:
        """
        Send an action
        Only clients can send actions

        :param action: Action
        :return: None
        """
        assert isinstance(action, Action)
        # logger.debug("Sending action: {}".format(action))
        action.on_sending(self)
        self.send(Message(data=action))


T = TypeVar('T', bound=LocalSession)


class Client(Generic[T], NetBase):
    """
    Network Client
    """

    def __init__(
            self,
            session: T,
            adapter: ClientAdapter,
            config: Optional[Dict[str, Any]] = None
    ) -> None:
        super().__init__(adapter=adapter, config=config)

        self._connected = False
        self._session = session

    @property
    def connected(self) -> bool:
        return self._connected

    @property
    def adapter(self) -> ClientAdapter:
        """
        "Casted" adapter instance
        :return: ClientAdapter
        """
        assert isinstance(self._adapter, ClientAdapter)
        return self._adapter

    @property
    def session(self) -> T:
        return self._session

    def connect(self, host: Optional[str] = None, port: Optional[int] = None) -> None:
        """
        Connect to the server
        This method does a bit of preparation and then calls `_run` to really start the process.

        :param host: str - Host to connect to
        :param port: int - Port to connect to on the host
        :return: None
        """

        if self.connected:
            logger.info("Already connected to {}:{}...".format(self.adapter.host, self.adapter.port))
            return

        self._session.on_connecting()

        self.adapter.host = host or self._config.get('host')
        self.adapter.port = port or self._config.get('port')

        logger.info("Connecting to {}:{}...".format(self.adapter.host, self.adapter.port))

        self._run()

    def disconnect(self) -> None:
        """
        Disconnect from the server
        This is only a semantic wrapper for the `_close` method.

        :return: None
        """

        logger.info("Disconnecting from server...")

        self._close()

    def step(self, now: float, dt: float) -> None:
        super().step(now, dt)

        if self._connected:
            self._session.step(now=now, dt=dt)

    def _on_message(self, message: Message) -> None:
        """
        Handle messages coming from the server
        If the message is a built-in type (i.e.: request, response, etc.) it will be handled here,
        otherwise, the message is passed to the to the session.

        :param message: Message
        :return: None
        """
        assert isinstance(message, Message)

        if message.data is None:
            logger.warning("Message has no data")
            return

        # NOTE: Checking by prefix is WAAYYY faster than with `isinstance`, we'll only assert the type for development
        message_prefix = message.data.serialization_prefix

        # if isinstance(message.data, Command) and not isinstance(message.data, ClientCommand):
        if message_prefix == SerializablePrefix.COMMAND or message_prefix == SerializablePrefix.BUILTIN_COMMAND:
            assert isinstance(message.data, Command) and not isinstance(message.data, ClientCommand)
            # Handle commands (Generic and Server)
            command = message.data
            command.handle(session=self._session)

        # elif isinstance(message.data, ServerRequest):
        elif message_prefix == SerializablePrefix.REQUEST:
            assert isinstance(message.data, ServerRequest)
            # Handle requests
            incoming_request = message.data

            def on_response(response: Response) -> None:
                assert isinstance(response, ClientResponse)
                response.request_id = incoming_request.id
                # logger.debug("Sending response: {}".format(response))
                response.on_sending(self)
                self.send(Message(data=response))

            incoming_request.handle(session=self._session, respond=on_response)

        # elif isinstance(message.data, ServerResponse):
        elif message_prefix == SerializablePrefix.RESPONSE:
            assert isinstance(message.data, ServerResponse)
            # Handle responses
            incoming_response = message.data
            if incoming_response.request_id is not None:
                try:
                    prior_request = self._requests.pop(incoming_response.request_id)
                except KeyError:
                    logger.warning("Received a response without a prior request (id: \"{}\")".format(incoming_response.request_id))
                else:
                    incoming_response.handle(request=prior_request, session=self._session)
            else:
                logger.warning("Received a response without a request id")

        # elif isinstance(message.data, ServerNotification):
        elif message_prefix == SerializablePrefix.NOTIFICATION:
            assert isinstance(message.data, ServerNotification)
            # Handle notifications
            notification = message.data
            notification.handle(session=self._session)

        else:
            # The rest goes to the session for custom messages
            self._session.on_message(message)

    def _on_internal_message(self, message_id: InternalMessageId, payload: Any) -> None:
        """
        Handle internal messages from the network adapter

        :param message_id: InternalMessageId
        :param payload: Any
        :return: None
        """

        if message_id is InternalMessageId.CONNECTED:
            logger.info("Connected")
            self._connected = True
            self.session.on_connected()

        elif message_id is InternalMessageId.DISCONNECTED:
            logger.info("Disconnected")
            self._connected = False
            self._close_connection()
            self._session.on_disconnected()
