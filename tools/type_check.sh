#!/bin/sh
export MYPYPATH=extlib/satlib
{ mypy --incremental --strict satnet; } | awk '!seen[$0]++'
